# Ansible playbook: Fivem

Installs, configure and secure FiveM server in RHEL/Fedora, Debian/Ubuntu.

FiveM is a modification for Grand Theft Auto V enabling you to play multiplayer on customized dedicated servers, powered by Cfx.re.

## Requirements

___Debian/Ubuntu Family___

    sudo apt-get -y install ansible git 

___RHEL/Fedora  Family___

    sudo yum -y install ansible git

## Role Variables

### Apache Configuration ###
    apache_listen_ip: "*"
    apache_listen_port: 8080
    apache_listen_port_ssl: 8443

### Mariadb configuration ###
    mysql_root_password: SecurePassword
__Memory settings (default values optimized ~512MB RAM).__

    mysql_key_buffer_size: "256M"
    mysql_max_allowed_packet: "64M"
    mysql_table_open_cache: "256"
    mysql_sort_buffer_size: "1M"
    mysql_read_buffer_size: "1M"
    mysql_read_rnd_buffer_size: "4M"
    mysql_myisam_sort_buffer_size: "64M"
    mysql_thread_cache_size: "8"
    mysql_query_cache_type: "0"
    mysql_query_cache_size: "16M"
    mysql_query_cache_limit: "1M"
    mysql_max_connections: "151"
    mysql_tmp_table_size: "16M"
    mysql_max_heap_table_size: "16M"
    mysql_group_concat_max_len: "1024"
    mysql_join_buffer_size: "262144"
__Other settings.__

    mysql_lower_case_table_names: "0"
    mysql_wait_timeout: "28800"
    mysql_event_scheduler_state: "OFF"
__InnoDB settings.__

    mysql_innodb_file_per_table: "1"
__Set buffer_pool_size up to 80% of RAM but beware of setting too high.__

    mysql_innodb_buffer_pool_size: "256M"
__Set log_file_size to 25% of buffer pool size.__

    mysql_innodb_log_file_size: "64M" 
    mysql_innodb_log_buffer_size: "8M"
    mysql_innodb_flush_log_at_trx_commit: "1"
    mysql_innodb_lock_wait_timeout: "50"
    mysql_mysqldump_max_allowed_packet: "64M"
__Databases.__

    mysql_databases:
       - name: fivem_db
         collation: utf8_general_ci
         encoding: utf8
__Users.__

    mysql_users:
       - name: fivem
         host: localhost
         password: SuperSecret
         priv: 'fivem_db.*:ALL'
__Mysql database backup.__

    mysql_backup_enabled: true
    mysql_backup_path: /data/backup/mariadb
    mysql_backup_frequency: daily
     
#### Php & PhpMyAdmin configuration ####
    phpMyAdmin_alias: phpmyadmin
    phpmyadmin_Autorized_ip: 
      - "0.0.0.1"
      - "0.0.0.2"
    htpasswd_credentials:
      - name: dede
        password: 'superpassword'

#### Fivem configuration ####
    dsg_user_password: "abc123DEF"
    LATEST_RECOMMENDED_FXSERVER: true
    fivem_dirname: fivem
    fivem_build_id: ~
    fivem_hostname: test fivem dev
    fivem_projectName: ''
    fivem_projectDesc: ''
    fivem_tag: 'DSG'
    fivem_lang: fr-FR
    fivem_steamid_admin: steam:1100001007f5666
    fivem_licenseKey: ajwmeorv9wof08h5ftwxkc6ki1pgqm2k
    fivem_steam_webApi: FDC11AB0A232DF71B8CC8A91EFB5B613
    fivem_max_players: 32
    mariadb_users: 'fivem'
    mariadb_root_password: 'SuperSecret'
    mariadb_databases: 'fivem_db'
    fivem_port: 30120
    txadmin_port: 40120


## Dependencies
  - dsg-role-firewall
  - dsg-role-fail2ban
  - dsg-role-apache
  - dsg-role-mariadb
  - dsg-role-php
  - dsg-role-phpmyadmin

## Example Playbook

    - hosts: localhost
      become: yes
      gather_facts: True
    
      roles:
        - { role: dsg-role-firewall }
        - { role: dsg-role-fail2ban }  
        - { role: dsg-role-apache }
        - { role: dsg-role-php }
        - { role: dsg-role-mariadb }
        - { role: dsg-role-phpmyadmin }    
        - { role: dsg-role-fivem }

## License

MIT / BSD

## Author Information

This role was created in 2021 by Didier MINOTTE.
