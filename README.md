## Ansible playbook: Fivem

[README English version](./README.EN.md)

## Description
Installation, configure et sécurisation d'un serveur FiveM sur RHEL/Fedora, Debian/Ubuntu.

FiveM est une modification pour Grand Theft Auto V vous permettant de jouer en multijoueur sur des serveurs dédiés personnalisés, alimentés par [Cfx.re](https://fivem.net/).

## Visuels
__Debian/Ubuntu__

Debian/Ubuntu Family
```bash
sudo apt-get -y install ansible git
git clone https://gitlab.com/DesiredServerGames/dsg-fivem-linux.git
cd dsg-fivem-linux
ansible-galaxy install -r requirements.yml
ansible-playbook -i localhost main.yml
```
RHEL/Fedora  Family
```bash
sudo yum -y install ansible git
git clone https://gitlab.com/DesiredServerGames/dsg-fivem-linux.git
cd dsg-fivem-linux
ansible-galaxy install -r requirements.yml
ansible-playbook -i localhost main.yml
```

## Role Variables

### Apache Configuration
Changer les valeurs des ports selon votre convenance
```yaml
apache_listen_ip: "*"
apache_listen_port: 8082
apache_listen_port_ssl: 9443
```
### Mariadb configuration
Changer la valeur par défaut par un mot de passe fort
```yaml
mysql_root_password: 'NETrupoGlVe6!Pa26bam'
```
__Paramètres de la mémoire (valeurs par défaut optimisées pour ~512MB RAM).__

Pour les valeurs des paramètres de la mémoire, laissez par défaut, un outil (MysqlTuner) est installer dans `/opt` pour vous permettre de régler ces valeurs plus finement plus tard
```yaml
mysql_key_buffer_size: "256M"
mysql_max_allowed_packet: "64M"
mysql_table_open_cache: "256"
mysql_sort_buffer_size: "1M"
mysql_read_buffer_size: "1M"
mysql_read_rnd_buffer_size: "4M"
mysql_myisam_sort_buffer_size: "64M"
mysql_thread_cache_size: "8"
mysql_query_cache_type: "0"
mysql_query_cache_size: "16M"
mysql_query_cache_limit: "1M"
mysql_max_connections: "151"
mysql_tmp_table_size: "16M"
mysql_max_heap_table_size: "16M"
mysql_group_concat_max_len: "1024"
mysql_join_buffer_size: "262144"
```
__Autres paramètres.__
```yaml
mysql_lower_case_table_names: "0"
mysql_wait_timeout: "28800"
mysql_event_scheduler_state: "OFF"
```
__InnoDB paramètres.__
```yaml
mysql_innodb_file_per_table: "1"
```
__Définissez la taille du buffer_pool_size jusqu'à 80 % de la RAM, mais attention à ne pas la fixer trop haut.__
```yaml
mysql_innodb_buffer_pool_size: "256M"
```
__Définir la taille du fichier journal à 25 % de la taille du pool de mémoire tampon.__
```yaml
mysql_innodb_log_file_size: "64M" 
mysql_innodb_log_buffer_size: "8M"
mysql_innodb_flush_log_at_trx_commit: "1"
mysql_innodb_lock_wait_timeout: "50"
mysql_mysqldump_max_allowed_packet: "64M"
```
__Base de données.__

Modifier les paramètres comme-ci dessous, m'oubliez pas de changer au minimin le nom de la base de donnée `name`
```yaml
mysql_databases:
   - name: gta5_fivem_db
     collation: utf8_general_ci
     encoding: utf8
```
__Utilisateur.__

Modifier les paramètres comme-ci dessous, m'oubliez pas de changer au minimin le nom de l'utilisateur `name` et `password` par un mot de passe bien plus fort
```yaml
mysql_users:
   - name: userfivemdb
     host: localhost
     password: 'cr-WEbRU!+B0ko*U?6oP'
     priv: 'fivem_db.*:ALL'
``` 
__Paramètre de la sauvegarde de la base de donnée.__

Pas besoin d'y toucher spécialement, sauvegarde de la base de donnée tout les jours
```yaml
mysql_backup_enabled: true
mysql_backup_path: /data/backup/mariadb
mysql_backup_frequency: daily
```
### PhpMyAdmin configuration
Modifier l'alias pour plus de sécurité lors de l'acces à votre page web de phpmyadmin "http://monipserveurpublic:lavaleurdeapache_listen_port/phpMyAdmin_alias"

exemple: "http://45.142.217.32:8080/phpmyadmin"

`phpmyadmin_Autorized_ip:` Sont les IP public autoriser à accéder à phpmyadmin

`htpasswd_credentials:` Est le compte autoriser à accéder à phpmyadmin (éviter de le donner à tous le monde :grin:)

```yaml
phpMyAdmin_alias: phpmyadmin
phpmyadmin_Autorized_ip: 
  - "45.142.217.32"
  - "75.142.117.12"
htpasswd_credentials:
  - name: antares
    password: 'yaBLfLb_e-ona7roQUMo'
```
### Fivem configuration
`LATEST_RECOMMENDED_FXSERVER:` installe la dernière recommander de fivem, passez le à false si vous désirez une version spécifique et indiquer le numéro de version du build dans le paramètre `fivem_build_id:`

N'oubliez pas de changer les valeurs de `mariadb_users` `mariadb_root_password` `mariadb_databases` par rapport à ce que vous avez mis plus haut dans la configuration de mariadb

Vous pouvez changer les ports de `fivem_port` et de `txadmin_port` si vous le désirez.

```yaml
dsg_user_password: "s@lBra=ad5troswivega"
LATEST_RECOMMENDED_FXSERVER: true
fivem_dirname: fivem
fivem_build_id: ~
fivem_hostname: test fivem dev
fivem_projectName: ''
fivem_projectDesc: ''
fivem_tag: 'DSG'
fivem_lang: fr-FR
fivem_steamid_admin: steam:1100001007f5666
fivem_licenseKey: ajwmeorv9wof08h5ftwxkc6ki1pgqm2k
fivem_steam_webApi: FDC11AB0A232DF71B8CC8A91EFB5B613
fivem_max_players: 32
mariadb_users: 'fivem'
mariadb_root_password: 'SuperSecret'
mariadb_databases: 'fivem_db'
fivem_port: 30120
txadmin_port: 40120
```

### Dependances
- dsg-role-firewall
- dsg-role-fail2ban
- dsg-role-apache
- dsg-role-mariadb
- dsg-role-php
- dsg-role-phpmyadmin

### Example Playbook
```yaml
  gather_facts: True

  roles:
    - { role: dsg-role-firewall }
    - { role: dsg-role-fail2ban }  
    - { role: dsg-role-apache }
    - { role: dsg-role-php }
    - { role: dsg-role-mariadb }
    - { role: dsg-role-phpmyadmin }    
    - { role: dsg-role-fivem }
```
### Support
Utiliser les issues prévu à cette effet

### Author Information
Ce rôle a été créé en 2021 par Didier MINOTTE.
